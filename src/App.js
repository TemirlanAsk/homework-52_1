import React, { Component } from 'react';
import './App.css';
import {rand} from "./random";
import  Number from './Numbers';

class App extends Component {
    state = {
        numbers:[]
    };


    generateNumber = () => {
        let i = 5;
        let numbers = [];
        while (i > 0) {
            let randomNumber = rand(5, 36);
            if (numbers.indexOf(randomNumber) === -1) {
                numbers.push(randomNumber);
                i--
            }
        }
        numbers.sort((a, b) => a > b);
        this.setState({numbers});


    };

    componentDidMount() {
        this.generateNumber();
    }

    render() {
        return (
            <div className="App">
                <div className="button"><button onClick={this.generateNumber}>Change number</button></div>
                <div className ="Nums">
                    <Number number={this.state.numbers[0]}/>
                    <Number number={this.state.numbers[1]}/>
                    <Number number={this.state.numbers[2]}/>
                    <Number number={this.state.numbers[3]}/>
                    <Number number={this.state.numbers[4]}/>
                </div>
            </div>
        );
    };
}
export default App;
